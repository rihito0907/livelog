//
//  UserRankingTableViewController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/12/01.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import Bolts
import SDWebImage

class UserRankingTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeNavigationBarMaterial()

        setupRefreshControl()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        let contentHeight = tableView.contentSize.height
        let lastPosition = tableView.contentOffset.y
    }
    
    // RefleshControlの設定
    private func setupRefreshControl() {
        guard let refreshControl = refreshControl else {
            return
        }
        refreshControl.addTarget(self, action: "refreshTable", forControlEvents: .ValueChanged)
    }
    
    // Navigation Bar Design
    private func makeNavigationBarMaterial() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0)
        navigationController?.navigationBar.layer.shadowOpacity = 0.5
        navigationController?.navigationBar.layer.shadowRadius = 2.0
        navigationController?.navigationBar.layer.shadowColor = UIColor.darkGrayColor().CGColor
    }
}