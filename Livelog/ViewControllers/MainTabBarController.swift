//
//  MainTabController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/18.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import SCLAlertView

class MainTabBarContoller: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self

        setupTabBar()
//        if UserDefaultsManager.hasRanking {
//            setRankingTab()
//        }


    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if viewController.isKindOfClass(DummyViewController) {
            guard let currentViewController = tabBarController.selectedViewController else {
                return false
            }
            guard let recordNavigationController = storyboard?.instantiateViewControllerWithIdentifier("RecordNavigationController") else {
                return false
            }
            let alert = SCLAlertView()
            alert.addButton("はい") { () -> Void in
                currentViewController.modalPresentationStyle = .OverCurrentContext
                currentViewController.presentViewController(recordNavigationController, animated: true, completion: { () -> Void in
                })
            }
            alert.addButton("いいえ") { () -> Void in
                
            }
            alert.showNotice("確認", subTitle: "計測を開始してもよろしいですか？")
        }
        else {
            self.selectedViewController = viewController
        }
        
        return false
    }
    
    func setRankingTab() {
        if UserDefaultsManager.hasRanking == false {
            return
        }
        
        guard let rankingNavigationController = storyboard?.instantiateViewControllerWithIdentifier("RankingNavigationController") else {
            return
        }
        rankingNavigationController.tabBarItem = UITabBarItem(title: "ランキング", image: UIImage(named: "ic_format_list_numbered_36pt"), tag: 3)
        
        viewControllers?.append(rankingNavigationController)
    }
    
    private func setupTabBar() {
        self.tabBar.tintColor = UIColor(red: 0/255.0, green: 203/255.0, blue: 222/255.0, alpha: 1.0)
    }
}
