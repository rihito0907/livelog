//
//  RecordingViewController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/10.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import Charts
import SCLAlertView
import Instructions

class RecordingViewController: UIViewController, CoachMarksControllerDataSource {
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var stopButton: MaterialButton!
    
    // チャート用の変数
    var dataCount = 0
    let dataLength = 120
    var accelerations: [Double] = []
    var xValues: [Int] = []
    
    var lineChartData = LineChartData()
    var lineChartDataSet = LineChartDataSet()
    
    var chartTimer = NSTimer()
    
    // 時間計測用の変数
    var startTime = NSDate()
    var currentTime = NSDate()
    
    // 音圧計測用
    let audioManager = AudioManager()
    
    // チュートリアル用
    private let coachForChart = "iPhoneが動きの大きさがリアルタイムでグラフに表示されます"
    private let coachForStopButton = "イベントが終わったら終了ボタンを押してください"
    private let coachForNextButton = "OK"
    var coachMarksController: CoachMarksController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // NavigationBar を透過
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
//        navigationController?.navigationBar.shadowImage = UIImage()
        
        // 音声計測開始
        audioManager.startUpdatingVolume(0.5)
        
        // デバイス計測を開始
        MotionManager.sharedInstance.startRecordingDeviceMotionObject(0.5)
        
        // 時間計測を開始
        startTime = NSDate()
        
        // チャートを初期化
        initializeChart()
        
        // チャート更新用タイマー
        chartTimer = NSTimer(timeInterval: 0.5, target: self, selector: "updateCurrentState", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(chartTimer, forMode: NSRunLoopCommonModes)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // チュートリアル
        setupTutorial()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    func updateCurrentState() {
        updateChart()
        updateTime()
    }
    
    // チャートの初期化
    private func initializeChart() {
        // チャートビューの設定
        chartView.backgroundColor = UIColor.whiteColor()
        
        chartView.noDataText = ""
        chartView.descriptionText = ""
        
        chartView.dragEnabled = true
        chartView.pinchZoomEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.drawMarkers = false
        chartView.legend.enabled = false
        
        // y軸を設定
        let yAxis = chartView.leftAxis
        yAxis.labelTextColor = UIColor.lightGrayColor()
        yAxis.labelPosition = .InsideChart
        yAxis.drawGridLinesEnabled = false
        yAxis.axisLineColor = UIColor.lightGrayColor()
        yAxis.labelCount = 5
        
        yAxis.axisMaxValue = 5.5
        yAxis.axisMinValue = 0.0
//        yAxis.customAxisMax = 5.5
//        yAxis.customAxisMin = 0.0
        
        // 右側のy軸を設定
        let yAxis2 = chartView.rightAxis
        yAxis2.drawGridLinesEnabled = false
        yAxis2.drawLabelsEnabled = false
        yAxis2.axisLineColor = UIColor.lightGrayColor()

        // x軸を設定
        let xAxis = chartView.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = true
        xAxis.axisLineColor = UIColor.lightGrayColor()
        
        // 初期値を設定
        xValues = ([Int])(0...dataLength)
        accelerations.append(0)
        let accelerationEnries = accelerations[max(0, dataCount - dataLength) ..< dataCount].enumerate().map({ (index: Int, element: Double) -> ChartDataEntry in
            ChartDataEntry(value: element, xIndex: index)
        })
        
        lineChartDataSet = LineChartDataSet(yVals: accelerationEnries, label: "加速度")
        // datasetの設定
        lineChartDataSet.drawCubicEnabled = true
        lineChartDataSet.cubicIntensity = 0.2
        lineChartDataSet.drawCirclesEnabled = false
        lineChartDataSet.lineWidth = 3.0
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        lineChartDataSet.drawValuesEnabled = false
        lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
        lineChartDataSet.colors = [UIColor(red: 255/255.0, green: 171/255.0, blue: 66/255.0, alpha: 1.0)]
        
        lineChartData = LineChartData(xVals: xValues, dataSet: lineChartDataSet)
        
        chartView.data = lineChartData
 
    }
    
    // チャートの更新
    func updateChart() {
        dataCount++
        
        let currentMotion = MotionManager.sharedInstance.currentMotion
        accelerations.append(currentMotion.accelerationAbs)
        let accelerationEnries = accelerations[max(0, dataCount - dataLength) ..< dataCount].enumerate().map({ (index: Int, element: Double) -> ChartDataEntry in
            ChartDataEntry(value: element, xIndex: index)
        })
        
        lineChartDataSet = LineChartDataSet(yVals: accelerationEnries, label: "加速度")
        // datasetの設定
        lineChartDataSet.drawCubicEnabled = true
        lineChartDataSet.cubicIntensity = 0.2
        lineChartDataSet.drawCirclesEnabled = false
        lineChartDataSet.lineWidth = 3.0
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        lineChartDataSet.drawValuesEnabled = false
        lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
        lineChartDataSet.colors = [UIColor(red: 255/255.0, green: 171/255.0, blue: 66/255.0, alpha: 1.0)]
        
        lineChartData = LineChartData(xVals: xValues, dataSet: lineChartDataSet)
        
        chartView.data = lineChartData
    }
    
    // 計測時間を更新
    func updateTime() {
        currentTime = NSDate()
        
        let deltaTime = currentTime.timeIntervalSinceDate(startTime)
        let hour = Int(deltaTime / (60 * 60))
        let minute = Int((deltaTime / 60) % 60)
        let second = Int(deltaTime % 60)
        
        let timeText = NSString(format: "%02d:%02d:%02d", hour, minute, second)
        timeLabel.text = timeText as String
    }
    
    // Stopボタン
    @IBAction func stopButtonPressed(sender: AnyObject) {
        let alert = SCLAlertView()
        alert.addButton("はい") { () -> Void in
            MotionManager.sharedInstance.stopRecordingDeviceMotionObject()
            self.audioManager.stopUpdatingVolume()
            // イベント登録画面に遷移
            self.currentTime = NSDate()
            

            
            self.performSegueWithIdentifier("MoveToAddEvent", sender: self)
        }
        alert.addButton("いいえ") { () -> Void in
        }
        alert.showNotice("確認", subTitle: "計測を終了してもよろしいですか？")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let identifier = segue.identifier else {
            return
        }
        switch identifier {
        case "MoveToAddEvent":
            guard let addEventViewController = segue.destinationViewController as? AddEventTableViewController else {
                return
            }
            addEventViewController.startTime = startTime
            addEventViewController.endTime = currentTime
            break
        default:
            break
        }
    }
    
    // チュートリアルを設定
    private func setupTutorial() {
        if UserDefaultsManager.tutorialRecord {
            UserDefaultsManager.tutorialRecord = false
            coachMarksController = CoachMarksController()
            guard let coachMarksController = coachMarksController else {
                return
            }
            coachMarksController.datasource = self
            coachMarksController.startOn(self)
        }
    }
    
    // CoachMarksControllerDataSource Methods
    func numberOfCoachMarksForCoachMarksController(coachMarksController: CoachMarksController) -> Int {
        return 2
    }
    
    func coachMarksController(coachMarksController: CoachMarksController, coachMarksForIndex index: Int) -> CoachMark {
        switch index {
        case 0:
            return coachMarksController.coachMarkForView(chartView, bezierPathBlock: nil)
        case 1:
            return coachMarksController.coachMarkForView(stopButton, bezierPathBlock: nil)
            
        default:
            return coachMarksController.coachMarkForView()
        }
    }
    
    func coachMarksController(coachMarksController: CoachMarksController, coachMarkViewsForIndex index: Int, coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        
        let coachViews = coachMarksController.defaultCoachViewsWithArrow(true, arrowOrientation: coachMark.arrowOrientation)
        
        switch(index) {
        case 0:
            coachViews.bodyView.hintLabel.text = coachForChart
            coachViews.bodyView.nextLabel.text = coachForNextButton
            break
        case 1:
            coachViews.bodyView.hintLabel.text = coachForStopButton
            coachViews.bodyView.nextLabel.text = coachForNextButton
        default:
            break
        }
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
}
