//
//  AddEventViewController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/04.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import SCLAlertView
import Instructions

class AddEventTableViewController: UITableViewController, CoachMarksControllerDataSource {
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var groupField: UITextField!
    @IBOutlet weak var startAtField: UITextField!
    @IBOutlet weak var endAtField: UITextField!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var positionCell: PositionTableViewCell!

    var startDatePicker: UIDatePicker!
    var endDatePicker: UIDatePicker!

    var startToolbar: UIToolbar!
    var endToolbar: UIToolbar!

    // 受け渡される日時
    var startTime = NSDate()
    var endTime = NSDate()

    // 登録するイベント
    var event = Event()

    // チュートリアル用
    private let coachForTextFields = "イベントのタイトル・グループ名を入力してください。また開始時刻・終了時刻も正しく調整してください。"
    private let coachForDoneButton = "入力が終わったら完了ボタンを押してください。計測をキャンセルする場合はキャンセルボタンを押してください"
    private let coachForNextButton = "OK"
    var coachMarksController: CoachMarksController?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.whiteColor()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        tableView.tableFooterView = UIView(frame: CGRectZero)

        initializeTextField()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        initializeTextFieldInput()

        // チュートリアルを設定
        setupTutorial()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func initializeTextField() {
        startAtField.text = dateToString(startTime)
        endAtField.text = dateToString(endTime)
    }

    func initializeTextFieldInput() {
        // 開始日時のTextFieldの入力にDatePickerを設定
        startDatePicker = UIDatePicker()
        startDatePicker.datePickerMode = .DateAndTime
        startDatePicker.backgroundColor = UIColor.whiteColor()
        startDatePicker.date = startTime

        startDatePicker.addTarget(self, action: "changedStartDate:", forControlEvents: .ValueChanged)

        startAtField.inputView = startDatePicker

        // 終了日時のTextFieldの入力にDatePickerを設定
        endDatePicker = UIDatePicker()
        endDatePicker.datePickerMode = .DateAndTime
        endDatePicker.backgroundColor = UIColor.whiteColor()
        endDatePicker.date = endTime

        endDatePicker.addTarget(self, action: "changedEndDate:", forControlEvents: .ValueChanged)

        endAtField.inputView = endDatePicker
    }

    func changedStartDate(sender: AnyObject?) {
        startAtField.text = dateToString(startDatePicker.date)
    }

    func changedEndDate(sender: AnyObject?) {
        endAtField.text = dateToString(endDatePicker.date)
    }

    // Finishing Events
    @IBAction func completeEditing(sender: AnyObject) {
        // DatePickerの時刻をstartTime, endTimeに設定
        startTime = startDatePicker.date
        endTime = endDatePicker.date
        // グループ名が入力されていなければreturn
        if groupField.text == nil || groupField.text == "" {
            return
        }
        // 開始時刻が終了時刻より遅いならreturn
        if startTime.timeIntervalSinceReferenceDate >= endTime.timeIntervalSinceReferenceDate {
            return
        }
        // タイトルがnilの場合は空文字
        if titleField.text == nil {
            titleField.text = ""
        }

        guard let doneButton = sender as? UIBarButtonItem else {
            return
        }
        // 連打防止
        doneButton.enabled = false

        let pointManager = PointManager(data: MotionManager.sharedInstance.currentMotions)
        pointManager.calculatePointUsingSum(startTime, endAt: endTime) {
            (point: Double) -> () in
            self.event.title = self.titleField.text!
            self.event.group = self.groupField.text!
            self.event.startAt = self.startTime
            self.event.endAt = self.endTime
            self.event.point = Int(point)

            self.event.positionX = self.positionCell.positionX.map {
                Double($0)
            } ?? -1
            self.event.positionY = self.positionCell.positionY.map {
                Double($0)
            } ?? -1

            EventManager.sharedInstance.addEvent(self.event, completion: {
                (error) -> Void in
                if error == nil {
                } else {
                    print("add event error: \(error)")
                    doneButton.enabled = true
                }
            })
            self.performSegueWithIdentifier("MoveToComplete", sender: self)
        }
    }

    @IBAction func cancelEditing(sender: AnyObject) {
        let alert = SCLAlertView()
        alert.addButton("はい") {
            () -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addButton("いいえ") {
            () -> Void in
        }
        alert.showWarning("注意", subTitle: "計測をキャンセルしてもよろしいですか？")
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let identifier = segue.identifier else {
            return
        }
        switch identifier {
        case "MoveToComplete":
            guard let completeViewController = segue.destinationViewController as? CompleteViewController else {
                return
            }
            completeViewController.event = event
            break
        default:
            break
        }
    }

    // チュートリアルを設定
    private func setupTutorial() {
        if UserDefaultsManager.tutorialAddEvent {
            UserDefaultsManager.tutorialAddEvent = false
            coachMarksController = CoachMarksController()
            guard let coachMarksController = coachMarksController else {
                return
            }
            coachMarksController.datasource = self
            coachMarksController.startOn(self)
        }
    }

    // CoachMarksControllerDataSource Methods
    func numberOfCoachMarksForCoachMarksController(coachMarksController: CoachMarksController) -> Int {
        return 2
    }

    func coachMarksController(coachMarksController: CoachMarksController, coachMarksForIndex index: Int) -> CoachMark {
        switch index {
        case 0:
            guard let navigationController = navigationController else {
                return coachMarksController.coachMarkForView()
            }
            let originOfTextFields = CGPointMake(tableView.frame.origin.x, tableView.frame.origin.y + navigationController.navigationBar.frame.size.height + UIApplication.sharedApplication().statusBarFrame.size.height)
            let sizeOfTextField = CGSizeMake(tableView.contentSize.width, 200)
            let rectOfTextFields = CGRect(origin: originOfTextFields, size: sizeOfTextField)
            let viewOfTextFields = UIView(frame: rectOfTextFields)
            return coachMarksController.coachMarkForView(viewOfTextFields, bezierPathBlock: nil)
        case 1:
            guard let navigationController = navigationController else {
                return coachMarksController.coachMarkForView()
            }
            return coachMarksController.coachMarkForView(navigationController.navigationBar, bezierPathBlock: nil)
        default:
            return coachMarksController.coachMarkForView()
        }
    }

    func coachMarksController(coachMarksController: CoachMarksController, coachMarkViewsForIndex index: Int, coachMark: CoachMark) -> (bodyView:CoachMarkBodyView, arrowView:CoachMarkArrowView?) {

        let coachViews = coachMarksController.defaultCoachViewsWithArrow(true, arrowOrientation: coachMark.arrowOrientation)

        switch (index) {
        case 0:
            coachViews.bodyView.hintLabel.text = coachForTextFields
            coachViews.bodyView.nextLabel.text = coachForNextButton
            break
        case 1:
            coachViews.bodyView.hintLabel.text = coachForDoneButton
            coachViews.bodyView.nextLabel.text = coachForNextButton
        default:
            break
        }

        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }

    // Helper Methods
    func dateToString(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP")
        dateFormatter.dateFormat = "yyyy年MM月dd日 HH時mm分"
        return dateFormatter.stringFromDate(date)
    }

}
