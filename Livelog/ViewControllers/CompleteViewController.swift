//
//  CompleteViewController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/14.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import Instructions

class CompleteViewController: UIViewController, CoachMarksControllerDataSource {
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var closeButton: MaterialButton!
    
    // チュートリアル用
    private let coachForPointLabel = "ここに今回のあなたの応援ポイントが表示されます"
    private let coachForCloseButton = "完了ボタンを押して測定を終了してください"
    private let coachForNextButton = "OK"
    var coachMarksController: CoachMarksController?
    
    var event = Event() {
        didSet {
            guard let infoLabel = infoLabel else {
                return
            }
            infoLabel.text = "今回の\(event.group)への応援ポイントは"
            guard let pointLabel = pointLabel else {
                return
            }
            pointLabel.text = "\(event.point)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        
        infoLabel.text = "今回の\(event.group)への応援ポイントは"
        pointLabel.text = "\(event.point)"
        
        // Realmの中の加速度のデータをサーバに送信
        NSOperationQueue().addOperationWithBlock { () -> Void in
            MotionManager.sharedInstance.sendDeviceMotionObject { () -> () in
            }
        }
        // Realmの中の音圧データをサーバに送信
        NSOperationQueue().addOperationWithBlock { () -> Void in
            AudioDataClient.sharedInstance.sendAudioData({ () -> () in
            })
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        setupTutorial()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func closeButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
        }
    }
    
    // チュートリアルを設定
    private func setupTutorial() {
        if UserDefaultsManager.tutorialComplete {
            UserDefaultsManager.tutorialComplete = false
            coachMarksController = CoachMarksController()
            guard let coachMarksController = coachMarksController else {
                return
            }
            coachMarksController.datasource = self
            coachMarksController.startOn(self)
        }
    }
    
    // CoachMarksControllerDataSource Methods
    func numberOfCoachMarksForCoachMarksController(coachMarksController: CoachMarksController) -> Int {
        return 2
    }
    
    func coachMarksController(coachMarksController: CoachMarksController, coachMarksForIndex index: Int) -> CoachMark {
        switch index {
        case 0:
            return coachMarksController.coachMarkForView(pointLabel, bezierPathBlock: nil)
        case 1:
            return coachMarksController.coachMarkForView(closeButton, bezierPathBlock: nil)
            
        default:
            return coachMarksController.coachMarkForView()
        }
    }
    
    func coachMarksController(coachMarksController: CoachMarksController, coachMarkViewsForIndex index: Int, coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        
        let coachViews = coachMarksController.defaultCoachViewsWithArrow(true, arrowOrientation: coachMark.arrowOrientation)
        
        switch(index) {
        case 0:
            coachViews.bodyView.hintLabel.text = coachForPointLabel
            coachViews.bodyView.nextLabel.text = coachForNextButton
            break
        case 1:
            coachViews.bodyView.hintLabel.text = coachForCloseButton
            coachViews.bodyView.nextLabel.text = coachForNextButton
        default:
            break
        }
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
}