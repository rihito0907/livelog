//
//  FeedTableViewController.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/03.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import SCLAlertView
import Instructions
import Bolts
import SDWebImage

class FeedTableViewController: UITableViewController, CoachMarksControllerDataSource {
    // チュートリアル用
    private let coachForRecordTab = "イベントが始まる前に計測を押してください"
    private let coachForNextButton = "OK"
    var coachMarksController: CoachMarksController?

    // Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        makeNavigationBarMaterial()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        setupTutorial()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    // CoachMarksControllerDataSource Methods
    private func setupTutorial() {
        if UserDefaultsManager.tutorialFeed {
            UserDefaultsManager.tutorialFeed = false
            self.coachMarksController = CoachMarksController()
            guard let coachMarksController = self.coachMarksController else {
                return
            }
            coachMarksController.datasource = self
            coachMarksController.startOn(self)
        }
    }

    func numberOfCoachMarksForCoachMarksController(coachMarksController: CoachMarksController) -> Int {
        return 1
    }

    func coachMarksController(coachMarksController: CoachMarksController, coachMarksForIndex index: Int) -> CoachMark {
        switch index {
        case 0:
            guard let tabBarController = self.tabBarController else {
                return coachMarksController.coachMarkForView()
            }
            return coachMarksController.coachMarkForView(tabBarController.tabBar, bezierPathBlock: nil)
        default:
            return coachMarksController.coachMarkForView()
        }
    }

    func coachMarksController(coachMarksController: CoachMarksController, coachMarkViewsForIndex index: Int, coachMark: CoachMark) -> (bodyView:CoachMarkBodyView, arrowView:CoachMarkArrowView?) {

        let coachViews = coachMarksController.defaultCoachViewsWithArrow(true, arrowOrientation: coachMark.arrowOrientation)

        switch (index) {
        case 0:
            coachViews.bodyView.hintLabel.text = coachForRecordTab
            coachViews.bodyView.nextLabel.text = coachForNextButton
            break
        default:
            break
        }

        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }

    // Navigation Bar Design
    private func makeNavigationBarMaterial() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.layer.shadowOffset = CGSizeMake(2.0, 2.0)
        navigationController?.navigationBar.layer.shadowOpacity = 0.5
        navigationController?.navigationBar.layer.shadowRadius = 2.0
        navigationController?.navigationBar.layer.shadowColor = UIColor.darkGrayColor().CGColor
    }
}