//
//  MaterialNavigationBar.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/12.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import QuartzCore

class MaterialNavigationBar: UINavigationBar {
    override func drawRect(rect: CGRect) {
        layer.shadowColor = UIColor.darkGrayColor().CGColor
        layer.shadowOffset = CGSizeMake(2.0, 2.0)
        layer.shadowOpacity = 0.8
        layer.masksToBounds = false
        layer.shouldRasterize = true
    }
}