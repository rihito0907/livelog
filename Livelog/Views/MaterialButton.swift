//
//  MaterialButton.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/10.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable class MaterialButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 30.0
    @IBInspectable var layerDepth: CGFloat = 2.0
    
    let shadowView = UIView()
    let shadowOpacity:Float = 0.5
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        // タップ時のボタンの動きを設定
        addTarget(self, action: "moveDown:", forControlEvents: .TouchDown)
        addTarget(self, action: "moveUp:", forControlEvents: [UIControlEvents.TouchUpInside, UIControlEvents.TouchUpOutside, UIControlEvents.TouchCancel])
        
        adjustsImageWhenHighlighted = false
    }
    
    override func drawRect(rect: CGRect) {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        
        // 影用のUIView
        shadowView.frame = frame
        shadowView.backgroundColor = UIColor.clearColor()
        shadowView.layer.masksToBounds = false
        shadowView.layer.backgroundColor = UIColor.clearColor().CGColor
        shadowView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        shadowView.layer.shadowOffset = CGSizeMake(layerDepth, layerDepth)
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).CGPath
        shadowView.layer.shadowOpacity = shadowOpacity
        shadowView.layer.shadowRadius = layerDepth
        shadowView.userInteractionEnabled = false
        
        // superviewに影用のUIViewを貼り付ける
        guard let superview = superview else {
            return
        }
        superview.insertSubview(shadowView, belowSubview: self)
    }
    
    func moveDown(sender: MaterialButton) {
        let delta = CGPointMake(layerDepth, layerDepth)
        sender.center += delta
        sender.shadowView.layer.shadowOpacity = 0
    }
    
    func moveUp(sender: MaterialButton) {
        let delta = CGPointMake(layerDepth, layerDepth)
        sender.center -= delta
        sender.shadowView.layer.shadowOpacity = shadowOpacity
    }
}
