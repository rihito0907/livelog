//
//  StopButton.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/11.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable class StopButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 30.0
    
    override func drawRect(rect: CGRect) {
        layer.masksToBounds = true
        layer.cornerRadius = cornerRadius
        
        // 影用のUIView
        let shadowView = UIView()
        shadowView.frame = frame
        shadowView.backgroundColor = UIColor.clearColor()
        shadowView.layer.masksToBounds = false
        shadowView.layer.backgroundColor = UIColor.clearColor().CGColor
        shadowView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        shadowView.layer.shadowOffset = CGSizeMake(2.0, 2.0)
        shadowView.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).CGPath
        shadowView.layer.shadowOpacity = 0.8
        //        shadowView.layer.shadowRadius = 2
        shadowView.userInteractionEnabled = false
        
        // superviewに影用のUIViewを貼り付ける
        guard let superview = superview else {
            return
        }
        superview.insertSubview(shadowView, belowSubview: self)
    }
}
