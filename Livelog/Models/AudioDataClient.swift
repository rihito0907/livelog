//
//  AudioDataClient.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/30.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import RealmSwift

class AudioDataClient {
    static let sharedInstance = AudioDataClient()
    
    private init() {
    }
    
    func sendAudioData(completion: () -> () ) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let realmPath = documentsPath + "/audio.realm"
        let realmURL = NSURL(string: realmPath)
        let realmConfig = Realm.Configuration(fileURL: realmURL)
        let realm = try! Realm(configuration: realmConfig)
        let audioDataArray = realm.objects(Audio)
        
        
        var requestDataArray: [AnyObject] = []
        for i in 0 ..< audioDataArray.count {
            let audioData = audioDataArray[i]

            var audioDic: [String: AnyObject] = [:]
            audioDic["uuid"] = UIDevice.currentDevice().identifierForVendor!.UUIDString
            audioDic["time_interval"] = audioData.createdAt
            audioDic["peak_power"] = audioData.peakPower
            audioDic["average_power"] = audioData.averagePower

            requestDataArray.append(audioDic)
        }
        
        let username = ""
        
        print(requestDataArray.count)
        
        LiveAPIClient.sharedInstance.postAudio(requestDataArray, username: username) { (error: NSError?) -> () in
            if error != nil {
                print("Error: \(error)")
            }
            else {
                print("Success: Audio has been saved.")
                self.deleteRealmAudios()
            }
        }
    }
    
    func deleteRealmAudios() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let realmPath = documentsPath + "/audio.realm"
        let realmURL = NSURL(string: realmPath)
        let realmConfig = Realm.Configuration(fileURL: realmURL)
        let realm = try! Realm(configuration: realmConfig)
        realm.beginWrite()
        realm.deleteAll()
        try! realm.commitWrite()
    }
}