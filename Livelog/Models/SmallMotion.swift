//
//  SmallMotion.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/14.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation

struct SmallMotion {
    var accelerationX = 0.0
    var accelerationY = 0.0
    var accelerationZ = 0.0
    var accelerationAbs: Double {
        get {
            return sqrt(accelerationX * accelerationX + accelerationY * accelerationY + accelerationZ * accelerationZ)
        }
    }
    var createdAt = NSDate()
}