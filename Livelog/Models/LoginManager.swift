//
//  LoginManager.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/12/03.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import Firebase

class LoginManager: NSObject {
    static let sharedInstance = LoginManager()

    private override init() {
    }

    // ログインチェック
    func checkCurrentUserAndLogIn() -> Bool {
//        if LivelogUser.currentUser()?.username == nil {
//            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//
//            let loginViewController = PFLogInViewController()
//            loginViewController.delegate = self
//            loginViewController.fields = .Twitter
//            loginViewController.logInView?.logo = UIView()
//            let logoImageView = UIImageView(frame: CGRectMake(0, 0, 300, 300))
//            logoImageView.image = UIImage(named: "Logo")
//            logoImageView.center = (appDelegate.window?.center)!
//            loginViewController.logInView!.addSubview(logoImageView)
//            appDelegate.window?.rootViewController = loginViewController
//            return false
//        }
//        else {
//            return true
//        }
        return false
    }

    func signInAnonymously() -> () {
        FIRAuth.auth()?.signInAnonymouslyWithCompletion() {
            (user, error) in
            let isAnonymous = user!.anonymous
            let uid = user!.uid
            print(isAnonymous.description + ", " + uid)
        }
    }

    // PFLogInViewControllerDelegate Methods
    // ログインできたらViewのユーザを登録
    // Crashlyticsに登録
//        if LivelogUser.currentUser()?.username != nil {
//        }
//        let mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MainTab")
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appDelegate.window?.rootViewController = mainViewController
//
//        UserInfo.sharedInstance.loadTwitterInfo(block: { (error: NSError?) -> () in
//            guard let user = LivelogUser.currentUser() else {
//                return
//            }
//            user.displayName = UserInfo.sharedInstance.name!
//            user.screenName = UserInfo.sharedInstance.screenName!
//            user.profileImageURL = UserInfo.sharedInstance.imageURL!
//            user.saveEventually()
//        })
}