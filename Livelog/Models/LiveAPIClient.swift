//
//  LiveAPIClient.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/17.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import Alamofire

class LiveAPIClient {
    static let sharedInstance = LiveAPIClient()
    private let baseURL = "http://52.68.200.175/live_activity/api/v1"
    
    private init () {}
    
    func postMotions(data: [AnyObject], username: String, completion:(NSError?) -> ()) {
        let parameters: [String: AnyObject] = [
            "data": data,
            "user_id": username
        ]
        
        let path = "/device_motions/update"
        
        let url = baseURL + path
        
        Alamofire.request(.POST, url, parameters: parameters, encoding: .JSON).responseJSON{ (response: Response<AnyObject, NSError>) -> Void in
            let result = response.result
            if result.isSuccess {
                completion(nil)
            }
            else {
                completion(result.error)
            }
        }
    }
    
    func postAudio(data: [AnyObject], username: String, completion:(NSError?) -> ()) {
        let parameters: [String: AnyObject] = [
            "data": data,
            "user_id": username
        ]
        
        let path = "/audios/update"
        
        let url = baseURL + path
        
        Alamofire.request(.POST, url, parameters: parameters, encoding: .JSON).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            let result = response.result
            if result.isSuccess {
                completion(nil)
            }
            else {
                completion(result.error)
            }
        }
    }
    
}