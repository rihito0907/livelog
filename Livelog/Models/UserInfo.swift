//
//  UserInfo.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/26.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation

class UserInfo {
    static let sharedInstance = UserInfo()
    
    var name: String? = ""
    var screenName: String? = ""
    var imageURL: String? = ""
    var ranking: Bool = false
    var share: Bool = false
    
    private init() {
    }

    func loadParseInfo(block: (NSError?) -> Void) {
//        guard let user = LivelogUser.currentUser() else {
//            return
//        }
//        guard let objectId = user.objectId else {
//            return
//        }
//        let query = LivelogUser.query()
//        query?.getObjectInBackgroundWithId(objectId, block: { (object: PFObject?, error: NSError?) -> Void in
//            if error == nil {
//                guard let resultUser = object as? LivelogUser else {
//                    return
//                }
//                self.ranking = resultUser.ranking
//                self.share = resultUser.share
//
//                UserDefaultsManager.hasRanking = resultUser.ranking
//                UserDefaultsManager.hasShare = resultUser.share
//            }
//            else {
//                self.ranking = UserDefaultsManager.hasRanking
//                self.share = UserDefaultsManager.hasShare
//            }
//            block(error)
//        })
    }


    func loadTwitterInfo(var userId: String = "", block: (NSError?) -> () ) {
//        guard let twitterUserId = PFTwitterUtils.twitter()?.userId else {
//            return
//        }
//        if userId == "" {
//            userId = twitterUserId
//        }
//
//        var urlString = "https://api.twitter.com/1.1/users/show.json?"
//        if userId.characters.count > 0 {
//            urlString += "user_id=" + userId
//        }
//        else {
//            return
//        }
//
//        guard let verify = NSURL(string: urlString) else {
//            return
//        }
//        let request = NSMutableURLRequest(URL: verify)
//        PFTwitterUtils.twitter()?.signRequest(request)
//
//        let queue = NSOperationQueue()
//        NSURLConnection.sendAsynchronousRequest(request, queue: queue) { (response: NSURLResponse?, data: NSData?, connectionError: NSError?) -> Void in
//            if connectionError == nil {
//                guard let data = data else {
//                    print("Data from Twitter is empty")
//                    return
//                }
//                do {
//                    guard let result = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject] else {
//                        return
//                    }
//                    self.name = result["name"] as? String
//                    self.screenName = result["screen_name"] as? String
//                    self.imageURL = result["profile_image_url_https"] as? String
//
//                    // UserDefaults に保存
//                    guard let name = self.name else {
//                        return
//                    }
//                    UserDefaultsManager.twitterName = name
//
//                    guard let screenName = self.screenName else {
//                        return
//                    }
//                    UserDefaultsManager.twitterScreenName = screenName
//
//                    guard let imageURL = self.imageURL else {
//                        return
//                    }
//                    UserDefaultsManager.twitterImageURL = imageURL
//
//                    block(nil)
//                }
//                catch let jsonError as NSError {
//                    print("JSON parsing error: \(jsonError)")
//                    block(jsonError)
//                }
//            }
//            else {
//                // 通信が失敗したら UserDefauls から取得
//                self.name = UserDefaultsManager.twitterName
//                self.screenName = UserDefaultsManager.twitterScreenName
//                self.imageURL = UserDefaultsManager.twitterImageURL
//
//                print("Connection Error: \(connectionError)")
//                block(connectionError)
//            }
//        }
        
    }
}