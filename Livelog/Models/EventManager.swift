//
//  EventManager.swift
//  Livelog
//
//  Created by Rihito on 2015/11/04.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit

class EventManager {
    static let sharedInstance = EventManager()
    
    var activityResult: [Event] = []
    var activityNetworking = false
    var activityCompleted = false
    
    var page = 0
    
    private init() {
    }
    
    func addEvent(event: Event, completion: (error: NSError?) -> Void ) {
//        addEvent(event.title, groupName: event.group, startAt: event.startAt, endAt: event.endAt, point: event.point, positonX: event.positionX, positionY: event.positionY) { (error) -> Void in
//            completion(error: error)
//        }
    }
    
    private func addEvent(title: String, groupName: String, startAt: NSDate, endAt: NSDate, point: Int, positonX: Double, positionY: Double, completion: (error: NSError?) -> Void ) {
//        let event = ParseEvent()
//
//        event.title = title
//        event.group = groupName
//        event.startAt = startAt
//        event.endAt = endAt
//        event.point = point
//        event.positionX = positonX
//        event.positionY = positionY
//        event.user = PFUser.currentUser()!
        
//        event.ACL = PFACL(user:PFUser.currentUser()!)
        
//        event.saveEventually().continueWithSuccessBlock { (task: BFTask) -> AnyObject? in
//            if task.completed {
//                print("event has been saved")
//            }
//            completion(error: task.error)
//            return nil
//        }
    }
    
    func loadActivity(reload: Bool, completion: (error: NSError?) -> Void) -> Bool {
//        let countPerPage = 1000
//
//        // リロード時はpageを0にする
//        if reload {
//            page = 0
//            activityResult.removeAll()
//            activityCompleted = false
//        }
//
//        // 通信中あるいはロードが終わっているならfalseを返す
//        if activityNetworking || activityCompleted {
//            return false
//        }
//
//        // クエリを発行
//        guard let query = ParseEvent.query() else {
//            return false
//        }
//        query.skip = page * countPerPage
//        query.orderByDescending("startAt")
//        // 通信できない場合はキャッシュから呼び出す
//        query.findObjectsInBackgroundWithBlock { (results: [PFObject]?, error: NSError? ) -> Void in
//            if error != nil {
//                print("Error: \(error!.description)")
//                return
//            }
//            if let results = results {
//                let events = results.map({ (result) -> ParseEvent in
//                    result as! ParseEvent
//                })
//                self.activityResult += events
//                // resultsが空なら検索終了
//                if results.count == 0 {
//                    self.activityCompleted = true
//                }
//            }
//            self.page++
//            completion(error: error)
//        }
        
        return true
    }
    
    func loadUserRanking() {
        
    }
}
