//
// Created by HashidoRihito on 15/05/29.
// Copyright (c) 2015 ___FULLUSERNAME___. All rights reserved.
//

#import "Livelog-Swift.h"
#import "AudioManager.h"

@implementation AudioManager {
    AudioQueueRef _queue;     // 音声入力用のキュー
    NSTimer *_timer;    // 監視タイマー
}

static void AudioInputCallback(
        void *inUserData,
        AudioQueueRef inAQ,
        AudioQueueBufferRef inBuffer,
        const AudioTimeStamp *inStartTime,
        UInt32 inNumberPacketDescriptions,
        const AudioStreamPacketDescription *inPacketDescs) {
    // 録音はしないので未実装
}

- (void)startUpdatingVolume:(NSTimeInterval)interval {

    // 記録するデータフォーマットを決める
    AudioStreamBasicDescription dataFormat;
    dataFormat.mSampleRate = 44100.0f;
    dataFormat.mFormatID = kAudioFormatLinearPCM;
    dataFormat.mFormatFlags = kLinearPCMFormatFlagIsBigEndian | kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
    dataFormat.mBytesPerPacket = 2;
    dataFormat.mFramesPerPacket = 1;
    dataFormat.mBytesPerFrame = 2;
    dataFormat.mChannelsPerFrame = 1;
    dataFormat.mBitsPerChannel = 16;
    dataFormat.mReserved = 0;

    // レベルの監視を開始する
    AudioQueueNewInput(&dataFormat, AudioInputCallback, (__bridge void *) (self), CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &_queue);
    AudioQueueStart(_queue, NULL);

    // レベルメータを有効化する
    UInt32 enabledLevelMeter = true;
    AudioQueueSetProperty(_queue, kAudioQueueProperty_EnableLevelMetering, &enabledLevelMeter, sizeof(UInt32));

    // 定期的にレベルメータを監視する
    _timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                              target:self
                                            selector:@selector(detectVolume:)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)stopUpdatingVolume {
    [_timer invalidate];
    // キューを空にして停止
    AudioQueueFlush(_queue);
    AudioQueueStop(_queue, NO);
    AudioQueueDispose(_queue, YES);
}

- (void)detectVolume:(NSTimer *)timer {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        // レベルを取得
        AudioQueueLevelMeterState levelMeter;
        UInt32 levelMeterSize = sizeof(AudioQueueLevelMeterState);
        AudioQueueGetProperty(_queue, kAudioQueueProperty_CurrentLevelMeterDB, &levelMeter, &levelMeterSize);

        double createdAt = [[NSDate date] timeIntervalSince1970];

        Audio *data = [[Audio alloc] init];
        data.peakPower = levelMeter.mPeakPower;
        data.averagePower = levelMeter.mAveragePower;
        data.createdAt = createdAt;

        // Realmの設定
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentPath = paths[0];
        NSString *realmPath = [NSString stringWithFormat:@"%@/audio.realm", documentPath];
        NSURL *realmURL = [NSURL URLWithString:realmPath];
        RLMRealmConfiguration *realmConfig = [RLMRealmConfiguration init];
        realmConfig.fileURL =realmURL;
        RLMRealm *realm = [RLMRealm realmWithConfiguration:realmConfig error:nil];
        [realm transactionWithBlock:^{
            [realm addObject:data];
        }];
    });
}

@end