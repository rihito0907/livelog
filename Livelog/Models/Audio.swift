//
// Created by HashidoRihito on 15/05/29.
// Copyright (c) 2015 ___FULLUSERNAME___. All rights reserved.
//

import Foundation
import RealmSwift

class Audio: Object {
    dynamic var peakPower: Double = 0
    dynamic var averagePower: Double = 0

    dynamic var createdAt: Double = 0
}
