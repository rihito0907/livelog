//
//  MotionManager.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/07.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import MotionKit
import RealmSwift
import CoreLocation

private let realmMotionPath = "motion"

class MotionManager {
    static let sharedInstance = MotionManager()
    private let motionKit = MotionKit()
    // 現在の値
    var currentMotion = SmallMotion()
    // ポイント計算用の加速度の配列
    var currentMotions: [SmallMotion] = []
    
    private init() {
    }
    
    func startRecordingDeviceMotionObject(interval: NSTimeInterval = 0.5) {
        // デバイスの情報を取得する
        motionKit.getDeviceMotionObject(interval) {
            deviceMotion -> () in
            let motionData = Motion()
            motionData.createdAt = NSDate().timeIntervalSince1970
            motionData.attitudeRoll = deviceMotion.attitude.roll
            motionData.attitudePitch = deviceMotion.attitude.pitch
            motionData.attitudeYaw = deviceMotion.attitude.yaw
            motionData.rotationX = deviceMotion.rotationRate.x
            motionData.rotationY = deviceMotion.rotationRate.y
            motionData.rotationZ = deviceMotion.rotationRate.z
            motionData.gravityX = deviceMotion.gravity.x
            motionData.gravityY = deviceMotion.gravity.y
            motionData.gravityZ = deviceMotion.gravity.z
            motionData.accelerationX = deviceMotion.userAcceleration.x
            motionData.accelerationY = deviceMotion.userAcceleration.y
            motionData.accelerationZ = deviceMotion.userAcceleration.z
            
            // 現在の値を更新
            self.currentMotion.accelerationX = motionData.accelerationX
            self.currentMotion.accelerationY = motionData.accelerationY
            self.currentMotion.accelerationZ = motionData.accelerationZ
            self.currentMotion.createdAt = NSDate(timeIntervalSince1970: motionData.createdAt)
            self.currentMotions.append(self.currentMotion)
            
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
            let realmPath = documentsPath + "/motion.realm"
            let realmURL = NSURL(string: realmPath)
            let realmConfig = Realm.Configuration(fileURL: realmURL)
            let realm = try! Realm(configuration: realmConfig)
            
            try! realm.write({ () -> Void in
                realm.add(motionData)
            })
        }
    }
    
    func stopRecordingDeviceMotionObject() {
        motionKit.stopDeviceMotionUpdates()
    }
    
    func sendDeviceMotionObject(completion:()->()) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let realmPath = documentsPath + "/motion.realm"
        let realmURL = NSURL(string: realmPath)
        let realmConfig = Realm.Configuration(fileURL: realmURL)
        let realm = try! Realm(configuration: realmConfig)
        
        let motionDataArray = realm.objects(Motion)
        
        var requestDataArray: [AnyObject] = []
        
        for i in 0 ..< motionDataArray.count {
            let motionData: Motion = motionDataArray[i] 
            
            var motionDic: [String:AnyObject] = [:]
            motionDic["uuid"] = UIDevice.currentDevice().identifierForVendor!.UUIDString
            motionDic["time_interval"] = motionData.createdAt
            motionDic["attitude_roll"] = motionData.attitudeRoll
            motionDic["attitude_pitch"] = motionData.attitudePitch
            motionDic["attitude_yaw"] = motionData.attitudeYaw
            motionDic["rotation_x"] = motionData.rotationX
            motionDic["rotation_y"] = motionData.rotationY
            motionDic["rotation_z"] = motionData.rotationZ
            motionDic["gravity_x"] = motionData.gravityX
            motionDic["gravity_y"] = motionData.gravityY
            motionDic["gravity_z"] = motionData.gravityZ
            motionDic["acceleration_x"] = motionData.accelerationX
            motionDic["acceleration_y"] = motionData.accelerationY
            motionDic["acceleration_z"] = motionData.accelerationZ
            
            requestDataArray.append(motionDic)
        }
        print(requestDataArray.count)
        
//        guard let user = PFUser.currentUser() else {
//            return
//        }
//        guard let username = user.username else {
//            return
//        }
//
//        LiveAPIClient.sharedInstance.postMotions(requestDataArray, username: username) { (error:NSError?) -> () in
//            if error != nil {
//                print("Error: \(error)")
//            }
//            else {
//                print("Success: Motion has been saved.")
//                self.deleteRealmMotions()
//                self.deleteCurrentMotions()
//            }
//        }
    }
    
    func deleteRealmMotions() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let realmPath = documentsPath + "/motion.realm"
        let realmURL = NSURL(string: realmPath)
        let realmConfig = Realm.Configuration(fileURL: realmURL)
        let realm = try! Realm(configuration: realmConfig)
        realm.beginWrite()
        realm.deleteAll()
        try! realm.commitWrite()
    }
    
    func deleteCurrentMotions() {
        currentMotions.removeAll()
    }
}