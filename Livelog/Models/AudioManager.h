//
// Created by HashidoRihito on 15/05/29.
// Copyright (c) 2015 ___FULLUSERNAME___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Realm/Realm.h>

@interface AudioManager : NSObject

- (void)startUpdatingVolume:(NSTimeInterval)interval;
- (void)stopUpdatingVolume;

@end