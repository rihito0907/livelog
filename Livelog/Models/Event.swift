//
//  Event.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/14.
//  Copyright © 2015年 Parse. All rights reserved.
//
import Foundation

struct Event {
    var title = ""
    var group = ""
    var startAt = NSDate()
    var endAt = NSDate()
    var point = 0
    var positionX = 0.0
    var positionY = 0.0
}