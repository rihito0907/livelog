//
//  LivelogUser.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/12/02.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import Firebase

class LivelogUser {
    var totalPoint: Int
    var eventCount: Int
    var screenName: String
    var profileImageURL: String
    var ranking: Bool
    var share: Bool

    init(){
        totalPoint = 0
        eventCount = 0
        screenName = ""
        profileImageURL = ""
        ranking = false
        share = false
    }
}