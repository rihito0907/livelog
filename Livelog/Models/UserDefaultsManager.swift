//
//  UserDefaultsManager.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/26.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation

struct UserDefaultsManager {
    private static let tutorialFeedKey = "tutorialFeed"
    private static let tutorialRecordKey = "tutorialRecordKey"
    private static let tutorialAddEventKey = "tutorialAddEventKey"
    private static let tutorialCompleteKey = "tutorialCompleteKey"
    private static let twitterNameKey = "twitterUserName"
    private static let twitterScreenNameKey = "twitterScreenName"
    private static let twitterImageURLKey = "twitterImageURL"
    private static let hasRankingKey = "hasRanking"
    private static let hasShareKey = "hasShare"

    private static let ud = NSUserDefaults.standardUserDefaults()

    static var tutorialFeed: Bool {
        get {
            return getValueBoolDefaultTrue(tutorialFeedKey)
        }
        set(newValue) {
            saveValue(tutorialFeedKey, value: newValue)
        }
    }

    static var tutorialRecord: Bool {
        get {
            return getValueBoolDefaultTrue(tutorialRecordKey)
        }
        set(newValue) {
            saveValue(tutorialRecordKey, value: newValue)
        }
    }

    static var tutorialAddEvent: Bool {
        get {
            return getValueBoolDefaultTrue(tutorialAddEventKey)
        }
        set(newValue) {
            saveValue(tutorialAddEventKey, value: newValue)
        }
    }

    static var tutorialComplete: Bool {
        get {
            return getValueBoolDefaultTrue(tutorialCompleteKey)
        }
        set(newValue) {
            saveValue(tutorialCompleteKey, value: newValue)
        }
    }

    static var twitterName: String {
        get {
            return getValueString(twitterNameKey)
        }
        set(newValue) {
            saveValue(twitterNameKey, value: newValue)
        }
    }

    static var twitterScreenName: String {
        get {
            return getValueString(twitterScreenNameKey)
        }
        set(newValue) {
            saveValue(twitterScreenNameKey, value: newValue)
        }
    }

    static var twitterImageURL: String {
        get {
            return getValueString(twitterImageURLKey)
        }
        set(newValue) {
            saveValue(twitterImageURLKey, value: newValue)
        }
    }

    static var hasRanking: Bool {
        get {
            return getValueBoolDefaultTrue(hasRankingKey)
        }
        set(newValue) {
            saveValue(hasRankingKey, value: newValue)
        }
    }

    static var hasShare: Bool {
        get {
            return getValueBoolDefaultTrue(hasShareKey)
        }
        set(newValue) {
            saveValue(hasShareKey, value: newValue)
        }
    }

    // Helper
    private static func getValueString(key: String) -> String {
        ud.registerDefaults([key: ""])
        guard let value = ud.objectForKey(key) as? String else {
            return ""
        }
        return value
    }

    private static func getValueBoolDefaultFalse(key: String) -> Bool {
        ud.registerDefaults([key: false])
        guard let value = ud.objectForKey(key) as? Bool else {
            return false
        }
        return value
    }

    private static func getValueBoolDefaultTrue(key: String) -> Bool {
        ud.registerDefaults([key: true])
        guard let value = ud.objectForKey(key) as? Bool else {
            return false
        }
        return value
    }

    private static func saveValue(key: String, value: AnyObject) {
        ud.setObject(value, forKey: key)
        ud.synchronize()
    }
}