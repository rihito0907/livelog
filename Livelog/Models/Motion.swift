//
//  Motion.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/07.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation
import RealmSwift

class Motion: Object {
    dynamic var attitudeRoll: Double = 0
    dynamic var attitudePitch: Double = 0
    dynamic var attitudeYaw: Double = 0
    
    dynamic var rotationX: Double = 0
    dynamic var rotationY: Double = 0
    dynamic var rotationZ: Double = 0
    
    dynamic var gravityX: Double = 0
    dynamic var gravityY: Double = 0
    dynamic var gravityZ: Double = 0
    
    dynamic var accelerationX: Double = 0
    dynamic var accelerationY: Double = 0
    dynamic var accelerationZ: Double = 0
    
    dynamic var createdAt: Double = 0
}