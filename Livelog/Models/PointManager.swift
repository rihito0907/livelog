//
//  PointManager.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/14.
//  Copyright © 2015年 Parse. All rights reserved.
//

import Foundation

class PointManager {
    let motions: [SmallMotion]
    
    init(data: [SmallMotion]) {
        motions = data
    }
    
    func calculatePointUsingSum(startAt: NSDate, endAt: NSDate, completion: Double -> ()) {
        let filteredData = motions.filter { (motion:SmallMotion) -> Bool in
            return (startAt.timeIntervalSinceReferenceDate <= motion.createdAt.timeIntervalSinceReferenceDate) && (motion.createdAt.timeIntervalSinceReferenceDate < endAt.timeIntervalSinceReferenceDate)
        }
        let accelerations = filteredData.map { (motion:SmallMotion) -> Double in
            motion.accelerationAbs
        }
        let point = calculatePointFromArray(accelerations)
        completion(point)
    }
    
    private func calculatePointFromArray(data: [Double]) -> Double {
        // 加速度の絶対値の合計値
        let accelerationSum = data.reduce(0, combine: {$0 + $1})
        
        //TODO: ポイント計算を実装
        let point = max(1, 1000 * accelerationSum / (2 * 60 * 10 * 2))
        
        return point
    }
}