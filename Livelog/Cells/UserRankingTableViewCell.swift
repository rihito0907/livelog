//
//  UserRankingTableViewCell.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/12/02.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit
import SDWebImage

class UserRankingTableViewCell: UITableViewCell{
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!

    var user: LivelogUser? {
        didSet {
            guard let user = user else {
                return
            }
            //TODO: rankingLabelの値
            profileImageView.sd_setImageWithURL(NSURL(string: user.profileImageURL), placeholderImage: UIImage())
//            nameLabel.text = user.displayName
            pointLabel.text = "\(user.totalPoint)"
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        //影を付ける
        mainView.layer.shadowOpacity = 0.5
        mainView.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        mainView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        mainView.layer.shadowRadius = 1
        // 画像を丸く
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2
        profileImageView.clipsToBounds = true
    }
}