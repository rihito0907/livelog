//
//  PositionTableViewCell.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/12/01.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit

class PositionTableViewCell: UITableViewCell {
    @IBOutlet weak var positionView: UIView!
    var touchCircle: UIView!
    
    var positionX: CGFloat?
    var positionY: CGFloat?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.userInteractionEnabled = true
        
        touchCircle = UIView(frame: CGRect(x: 0, y: 0, width: 20.0, height: 20.0))
        touchCircle.backgroundColor = UIColor(red: 255/255.0, green: 171/255.0, blue: 66/255.0, alpha: 1.0)
        touchCircle.layer.cornerRadius = 10.0
        touchCircle.clipsToBounds = true
        touchCircle.hidden = true
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        positionView.layer.borderColor = UIColor.darkGrayColor().CGColor
        positionView.layer.borderWidth = 1.0
        
        positionView.addSubview(touchCircle)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        touchCircle.hidden = false
        
        let location = touch.locationInView(self.positionView)
        
        if location.x < 0 || positionView.frame.size.width < location.x || location.y < 0 || positionView.frame.size.height < location.y {
            return
        }
        
        positionX = location.x / positionView.frame.size.width
        positionY = location.y / positionView.frame.size.height
        
        touchCircle.center = location
    }
}
