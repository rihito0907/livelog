//
//  FeedTableViewCell.swift
//  Livelog
//
//  Created by HashidoRihito on 2015/11/06.
//  Copyright © 2015年 Parse. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var twitterButton: UIButton!
    
    var event: Event? {
        didSet {
            guard let event = event else {
                return
            }
            mainLabel.text = "\(event.group)への応援ポイントは"
            pointLabel.text = "\(event.point) ポイント"
            titleLabel.text = event.title
            
            let timeFormatter = NSDateFormatter()
            timeFormatter.locale = NSLocale(localeIdentifier: "ja_JP")
            timeFormatter.dateFormat = "HH:mm"
            startLabel.text = timeFormatter.stringFromDate(event.startAt)
            endLabel.text = timeFormatter.stringFromDate(event.endAt)
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "ja_JP")
            dateFormatter.dateFormat = "YYYY/MM/dd"
            dateLabel.text = dateFormatter.stringFromDate(event.startAt)
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        //影を付ける
        mainView.layer.shadowOpacity = 0.5
        mainView.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        mainView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        mainView.layer.shadowRadius = 1
        // Twitter共有ボタンを丸く
        twitterButton.layer.cornerRadius = twitterButton.frame.width / 2
        twitterButton.clipsToBounds = true
        
        twitterButton.hidden = !UserDefaultsManager.hasShare
    }
}